<?php


return [
    '/' => [
        'target' => 'HomeController@index',
        'middleware' => \Application\Middlewares\AuthMiddleware::class
    ],

    "/user/1" =>[
        'target' => 'UsersController@insert',
    ],

];

