<?php

namespace System\Router;



class Router
{
    protected static $instance;

    public static function instance()
    {

        if(is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
         $this->boot();
    }


    private function getCurrentRoute()
    {
        return strtok($_SERVER['REQUEST_URI'],'?');
    }

    private function checkRouteExist($route)
    {
        $routes = include 'routes.php';

        if(in_array($route,array_keys($routes))){
            $target = $routes[$route]['target'];

            if(isset($routes[$route]['middleware'])){
                $middleware = $routes[$route]['middleware'];
                if(!empty($middleware)){
                    $middlewareInstance = new $middleware;
                }
            }

            list($controller,$method) = explode('@',$target);
            $classPath = "Application\\Controllers\\{$controller}";
            $classInstance = new $classPath;
            $request = new \System\Http\Request();
            $classInstance->{$method}($request);
            exit;
        }else{
            echo "Not Define Route";
            exit;
        }
    }

    private function boot()
    {
        $route = $this->getCurrentRoute();
        $this->checkRouteExist($route);
    }

    public function __clone()
    {
        exit;
    }


}