<?php
/**
 * Created by PhpStorm.
 * User: Amir
 * Date: 7/24/2018
 * Time: 2:02 PM
 */

namespace Application\Middlewares\Contracts;


abstract class Middleware
{

    abstract protected function handle();

    public function __construct()
    {
        $this->handle();
    }
}